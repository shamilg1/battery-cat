package im.gtm.batterycat;

import java.io.Serializable;

/**
* (c) 2015 Shamil Gumirov (shami@gumirov.com).<br/> Date: 7/31/15 Time: 5:12 PM<br/>
*/
public class ProcStat
  implements Serializable
{
  final int cpu;
  final String pid;
  final String pkg;
  final String name;
  final String pcy;
  final String uid;

  ProcStat(int cpu, String pid, String pkg, String name, String pcy, String uid)
  {
    this.cpu = cpu;
    this.pid = pid;
    this.pkg = pkg;
    this.name = name;
    this.pcy = pcy;
    this.uid = uid;
  }

  @Override
  public boolean equals(Object o)
  {
    return o instanceof ProcStat && pkg !=null&& pkg.equals(((ProcStat)o).pkg);
  }

  @Override
  public int hashCode()
  {
    return pkg.hashCode();
  }

  @Override
  public String toString()
  {
    return name;
  }
}
