package im.gtm.batterycat;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * (c) 2015 Shamil Gumirov (shami@gumirov.com).<br/> Date: 7/22/15 Time: 12:34 AM<br/>
 */
public class MonitorService
  extends Service
{
  public static final String ACTION = "im.gtm.batterycat.MONITOR_SERVICE";
  private boolean isRegisterBattery;
  private static final int SIMPLE_NOTFICATION_ID = 12873;
  private int count = 0;

  SharedPreferences sp;

  Set<ProcStat> blackList = new HashSet<ProcStat>();
  Set<ProcStat> whiteList = new HashSet<ProcStat>();
  private final Object listWriteMon = new Object();
  public static final String BLACK = "black";
  public static final String WHITE = "white";
  private static final int MAX_STATS = 512;

  private volatile boolean stopService;

  public static final String CPU_LOAD_ACTION = "gtm.im.cat.CPU_USER_LOAD";

  public static final String MODE_KEY = "mode";
  public static final int PARANOID = 0, VIGILANTE = 1, FULL_METAL = 2, SLEEPY_CAT = 3;
  public static final int[] MODES = new int[]{2, 12, 45, 100};
  private volatile int maxCpuDrain = MODES[VIGILANTE];

  /**
   * WindowManager Parameters for dialog screen.
   */
  WindowManager.LayoutParams par = new WindowManager.LayoutParams(
    ViewGroup.LayoutParams.WRAP_CONTENT,
    ViewGroup.LayoutParams.WRAP_CONTENT, 0, 0,
    WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
      | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
      | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
      | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
      | WindowManager.LayoutParams.FLAG_TOUCHABLE_WHEN_WAKING
    ,
    PixelFormat.RGBA_8888);
  private static final String REFRESH_ACTION = "im.gtm.cat.REFRESH";
  private BroadcastReceiver refreshReceiver = new BroadcastReceiver()
  {
    @Override
    public void onReceive(Context context, Intent intent)
    {
      refreshRules();
    }
  };

  public static final String SETTINGS = "settings";
  private SharedPreferences.OnSharedPreferenceChangeListener prefsListener = new SharedPreferences.OnSharedPreferenceChangeListener()
  {
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
      if (MODE_KEY.equals(key))
      {
        maxCpuDrain = MODES[sp.getInt(MODE_KEY, VIGILANTE)];
      }
    }
  };

  {
    par.gravity = Gravity.CENTER;
  }

  @Override
  public IBinder onBind(Intent intent)
  {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }

  int id;
  Runnable drainer = new Runnable()
  {
    @Override
    public void run()
    {
      while(true){
        String s="";
        for (int i = 0; i < 10; ++i) s+=i+s;
      }
    }
  };

  @Override
  public void onDestroy()
  {
    super.onDestroy();
    ((BatteryCatApplication)getApplicationContext()).serviceObservable.notifyObservers(null);
    stopService = true;
    unregisterReceiver(battReceiver);
    unregisterReceiver(refreshReceiver);
    if (sp != null) sp.unregisterOnSharedPreferenceChangeListener(prefsListener);
  }

  @Override
  public void onCreate()
  {
    super.onCreate();
    sp = getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
    sp.registerOnSharedPreferenceChangeListener(prefsListener);
    refreshRules();
    IntentFilter f = new IntentFilter();
    f.addAction(REFRESH_ACTION);
    registerReceiver(refreshReceiver, f);
    ((BatteryCatApplication)getApplicationContext()).serviceObservable.notifyObservers(this);
    maxCpuDrain = MODES[sp.getInt(MODE_KEY, VIGILANTE)];
  }

  private void refreshRules()
  {
    whiteList = load(this, WHITE);
    blackList = load(this, BLACK);
    Log.e("cpu", "updated rules: w="+whiteList.size()+" b="+blackList.size());
  }

  Runnable topProcess = new Runnable()
  {
    @Override
    public void run()
    {
      stopService = false;
      int count = 0;
      List<ProcStat> stats = new ArrayList<ProcStat>();
      while (!stopService)
      {
        stats.clear();
        try
        {
          ProcessBuilder builder = new ProcessBuilder("top", "-n", "1", "-s", "cpu", "-m", "10", "-d", "1");
          builder.redirectErrorStream(true);
          Process sh = builder.start();
          InputStreamReader isr = new InputStreamReader(sh.getInputStream());
          DataOutputStream os = new DataOutputStream(sh.getOutputStream());
          os.close();
          int code = sh.waitFor();
          BufferedReader br = new BufferedReader(isr);
          String line;
          //indexes of columns.
          int CPU=-1, PID=-1, PCY=-1, NAME=-1, UID=-1;
          while ((line = br.readLine()) != null) {
            line = line.trim();
            Log.d("top", line);
            if (line.contains("PID"))
            {
              String[] t = line.toLowerCase().split(" +");
              CPU = index("cpu", t, 1);
              PID = index("pid", t, 0);
              PCY = index("pcy", t, 6);
              NAME = index("name", t, 8);
              UID = index("uid", t, 7);
              continue;
            }
            else if (!line.isEmpty() && line.contains("%"))
            {
              String[] t = line.split(" +");
              if (t.length > max(CPU,PID,NAME,PCY,UID) && allGood(CPU, PID, NAME, PCY, UID))
              {
                String s = t[CPU].endsWith("%")?t[CPU].substring(0,t[CPU].length()-1) : t[CPU];
                int cpu = Integer.parseInt(s);
                stats.add(new ProcStat(cpu, t[PID], t[NAME],
                  getAppName(MonitorService.this, Integer.parseInt(t[PID]), t[NAME]), t[PCY], t[UID]));
                count = 0;
              } else if (t.length > 3)
              {
                Log.e("cpu", line);
                line = line.toLowerCase();
                if (line.contains("user ") && line.contains("%"))
                {
                  int uI = line.indexOf("user "), percI = line.indexOf("%", uI);
                  if (percI > uI)
                  {
                    int userLoad = Integer.parseInt(line.substring(uI+5, percI));
                    Intent currentCpuLoadIntent = new Intent(CPU_LOAD_ACTION).putExtra("cpuload", userLoad);
                    sendBroadcast(currentCpuLoadIntent);
                  }
                }
              }
            }
          }
          sh.destroy();
          Log.d("cpu", "top returned code="+code+" stats.len="+stats.size());
        } catch (Throwable e)
        {
          Log.e("cpu", "error while executing top command", e);
          count++;
        }
        for (ProcStat s : stats)
        {
          if (s.cpu > maxCpuDrain)
          {
            ProcStat app = s;
            if (!"fg".equals(app.pcy) && blackList.contains(app))
            {
              killApp(app);
            }
            else if (!"fg".equals(app.pcy) && !whiteList.contains(app))
              showAppKillDialog("Teach me!", "Background app " + getAppName(MonitorService.this, Integer.parseInt(s.pid), app.pkg) + " draining battery in bg (uses cpu at " + app.cpu + "%)", app);
          }
        }
        if (count == 20) Log.e("cpu", "too many errors, will not execute top process");
        try{ Thread.sleep(2000); }catch(Throwable e){}
      }
    }
  };

  private int max(int... v)
  {
    int max = v[0];
    for (int i : v) if (max < i) max = i;
    return max;
  }

  private boolean allGood(int ... vals)
  {
    for (int i : vals) if (i==-1) return false;
    return true;
  }

  private int index(String colname, String[] s, int defVal)
  {
    int slen = s.length;
    for (int i = 0; i < slen; ++i) if (s[i].contains(colname)) return i;
    return defVal;
  }

  public static String getAppName(Context c, int pID, String defaultName)
  {
    String processName = defaultName;
    ActivityManager am = (ActivityManager)c.getSystemService(ACTIVITY_SERVICE);
    List l = am.getRunningAppProcesses();
    Iterator i = l.iterator();
    PackageManager pm = c.getPackageManager();
    while(i.hasNext())
    {
      ActivityManager.RunningAppProcessInfo info = (ActivityManager.RunningAppProcessInfo)(i.next());
      try
      {
        if(info.pid == pID)
        {
//          CharSequence c = pm.getApplicationLabel(pm.getApplicationInfo(info.processName, PackageManager.GET_META_DATA));
          //Log.d("Process", "Id: "+ info.pid +" ProcessName: "+ info.processName +"  Label: "+c.toString());
          //processName = c.toString();
          processName = info.processName;
        }
      }
      catch(Exception e)
      {
        //Log.d("Process", "Error>> :"+ e.toString());
      }
    }
    return processName;
  }

  View dialog;
  boolean showing;

  private void showAppKillDialog(final String title, final String msg, final ProcStat app)
  {
    if (showing) return;
    Log.e("cpu", "showAppKillDialog()");
    final WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
    LayoutInflater inf = (LayoutInflater)getSystemService (Context.LAYOUT_INFLATER_SERVICE);

    if (dialog == null)
    {
      dialog = inf.inflate(R.layout.dialog, null);
    }
    else
    {
    }

    dialog.findViewById(R.id.kill).setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View view)
      {
        killApp(app);
        showing = false;
        if (((CheckBox)dialog.findViewById(R.id.remember)).isChecked()) addBlackList(app);
        wm.removeView(dialog);
      }
    });

    dialog.findViewById(R.id.hide).setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View view)
      {
        showing = false;
        if (((CheckBox)dialog.findViewById(R.id.remember)).isChecked()) addWhiteList(app);
        wm.removeView(dialog);
      }
    });

    new Handler(Looper.getMainLooper()).post(new Runnable()
    {
      @Override
      public void run()
      {
        ((CheckBox)dialog.findViewById(R.id.remember)).setChecked(false);
        ((TextView)dialog.findViewById(R.id.title)).setText(title);
        ((TextView)dialog.findViewById(R.id.message)).setText(msg);
        if (!showing) {
          showing = true;
          wm.addView(dialog, par);
        }
        dialog.setVisibility(View.VISIBLE);
      }
    });
  }

  private void addWhiteList(ProcStat app)
  {
    synchronized (WHITE){
      whiteList.add(app);
    }
    new Thread(new Runnable()
    {
      public void run()
      {
        save(MonitorService.this, WHITE, whiteList);
      }
    }).start();
  }

  private void addBlackList(ProcStat app)
  {
    synchronized (BLACK){
      blackList.add(app);
    }
    new Thread(new Runnable()
    {
      public void run()
      {
        save(MonitorService.this, BLACK, blackList);
      }
    }).start();
  }

  public static void save(Context c, String name, Set<ProcStat> list)
  {
    FileOutputStream fos = null;
    ObjectOutputStream bos = null;
    try
    {
      fos = c.openFileOutput(name + ".dat", MODE_PRIVATE);
      bos = new ObjectOutputStream(new BufferedOutputStream(fos));
      synchronized (name)
      {
        bos.writeInt(list.size());
        for (ProcStat s : list)
        {
          bos.writeObject(s);
        }
        bos.flush();
      }
      Log.d("cpu", "saved list ["+name+"]: "+list.size()+" items");
    }
    catch (IOException e)
    {
      Log.e("cpu", "cannot save '"+name+"'", e);
    }
    finally
    {
      sclose(bos);
      sclose(fos);
    }
    sendRefreshIntent(c);
  }

  private static void sendRefreshIntent(Context c)
  {
    Intent i = new Intent(REFRESH_ACTION);
    c.sendBroadcast(i);
  }

  public static Set<ProcStat> load(Context c, String name)
  {
    FileInputStream fis = null;
    ObjectInputStream bis = null;
    HashSet<ProcStat> s = new HashSet<ProcStat>();
    try
    {
      fis = c.openFileInput(name + ".dat");
      bis = new ObjectInputStream(new BufferedInputStream(fis));
      int len = bis.readInt();
      for (int i = 0; i < len; ++i)
      {
        ProcStat stat = (ProcStat) bis.readObject();
        s.add(stat);
      }
    }
    catch (Throwable e)
    {
      Log.d("cpu", "cannot load '"+name+"'", e);
    }
    finally
    {
      sclose(bis);
      sclose(fis);
    }
    return s;
  }

  private static void sclose(InputStream is)
  {
    try{ is.close(); }catch(Throwable ignore){}
  }

  private static void sclose(OutputStream os)
  {
    if (os != null) {
      try{ os.flush(); }catch(Throwable ignore){}
      try{ os.close(); }catch(Throwable ignore){}
    }
  }

  private void killApp(ProcStat app)
  {
    Log.e("cpu", "killapp ("+app.pkg +")");
    ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
    activityManager.killBackgroundProcesses(app.pkg);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId)
  {
    id = startId;
    if (!isRegisterBattery) {
      IntentFilter battFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
      registerReceiver(battReceiver, battFilter);
      isRegisterBattery = true;
    }

    new Thread(topProcess).start();
    return Service.START_REDELIVER_INTENT;
  }

  private static final double MAX_SPEED = 0.0926;
  private BroadcastReceiver battReceiver = new BroadcastReceiver() {
    public void onReceive(Context context, Intent intent) {
      Log.e("cpu", "battery level changed");
      int rawlevel = intent.getIntExtra("level", -1);
      int scale = intent.getIntExtra("scale", -1);

      if (rawlevel >= 0 && scale > 0) {
        double battLevel = (rawlevel * 100) / scale;
        double speed = calcSpeed(battLevel);
        if (speed > MAX_SPEED) showCurrentProcesses(speed);
      }
    }
  };

  private void showCurrentProcesses(double batteryDrainSpeed)
  {
    showNotification(getString(R.string.app_name), "Current battery drain speed: "+batteryDrainSpeed+" %/hr");
  }

  private void showNotification(String title, String msg)
  {
    Log.e("cpu", "showNotification() msg="+msg);
    NotificationManager notificationManager =
      (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
    final Notification notifyDetails =
      new Notification(R.drawable.ic_process_system,
        msg,System.currentTimeMillis());

    Context context = getApplicationContext();
    CharSequence contentTitle = title;
    CharSequence contentText = msg;
    Intent notifyIntent = new Intent(this, RulesActivity.class);
    PendingIntent intent = PendingIntent.getActivity(this, 0, notifyIntent,
      android.content.Intent.FLAG_ACTIVITY_NEW_TASK);

    notifyDetails.setLatestEventInfo(context, contentTitle, contentText, intent);
    notificationManager.notify(SIMPLE_NOTFICATION_ID, notifyDetails);
  }

  LinkedList<BatteryMetering> batteryStats = new LinkedList<BatteryMetering>();

  private double calcSpeed(double battLevel)
  {
    //todo make multiple points approximation
    BatteryMetering latest = null;
    if (batteryStats.size() > 0) latest = batteryStats.getLast(); else return 0;

    if (latest.level != battLevel)
    {
      BatteryMetering currentBattery = new BatteryMetering(battLevel);
      batteryStats.add(currentBattery);
      if (batteryStats.size() > MAX_STATS) batteryStats.removeFirst();
      double oldL = latest.level;
      double dt = (currentBattery.time - latest.time) / 3600000;
      double speed = (oldL - battLevel) / dt;
      Log.e("cpu", "speed="+speed);
      return speed;
    }
    return 0; //no level change
  }

  class BatteryMetering {
    double level;
    long time = System.currentTimeMillis();

    BatteryMetering(double level)
    {
      this.level = level;
    }
  }
}

