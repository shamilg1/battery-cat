package im.gtm.batterycat;

import android.app.Application;

import java.util.Observable;
import java.util.Observer;

/**
 * (c) 2015 Shamil Gumirov (shami@gumirov.com).<br/> Date: 8/3/15 Time: 10:01 PM<br/>
 */
public class BatteryCatApplication
  extends Application implements Observer
{
  public final Observable serviceObservable = new Observable(){
    @Override
    public void notifyObservers(Object data)
    {
      if (service != data) setChanged();
      service = (MonitorService) data;
      super.notifyObservers(data);
      clearChanged();
    }
  };
  private MonitorService service;

  @Override
  public void onCreate()
  {
    super.onCreate();
    serviceObservable.addObserver(this);
  }

  @Override
  public void update(Observable observable, Object o)
  {
    service = (MonitorService) o;
  }

  public MonitorService getService()
  {
    return service;
  }

  @Override
  public void onTerminate()
  {
    super.onTerminate();
    serviceObservable.deleteObserver(this);
  }
}
