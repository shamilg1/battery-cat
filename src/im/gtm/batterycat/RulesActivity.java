package im.gtm.batterycat;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Set;

/**
 * (c) 2015 Shamil Gumirov (shami@gumirov.com).<br/> Date: 7/22/15 Time: 2:22 AM<br/>
 */
public class RulesActivity
  extends Activity
{
  public static final String ACTION = "im.gtm.batterycat.CONSUMERS_LIST";
//  SharedPreferences sp;
  Handler h;
  private boolean paused;
  ListView wlist;
  ListView blist;
  ProcStat[] wstats;
  ProcStat[] bstats;
  Set<ProcStat> wset;
  Set<ProcStat> bset;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.rules);

    setTitle(getString(R.string.app_name)+" - rules list");

//    sp = getSharedPreferences("batt_levels", Context.MODE_PRIVATE);

    h = new Handler();
    wlist = (ListView) findViewById(R.id.wlist);
    blist = (ListView) findViewById(R.id.blist);
    registerForContextMenu(wlist);
    registerForContextMenu(blist);
  }

  @Override
  protected void onResume()
  {
    super.onResume();
    paused = false;
    updater.run();
  }

  @Override
  protected void onPause()
  {
    super.onPause();
    paused = true;
  }

  Runnable updater = new Runnable()
  {
    @Override
    public void run()
    {
      if (paused) return;
      updateLists();
      h.postAtTime(updater, SystemClock.uptimeMillis() + 5000);
      Log.d("cpu", "updateUI()");
    }
  };

  private void updateLists()
  {
    //experimental
    wset = MonitorService.load(this, MonitorService.WHITE);
    wstats = wset.toArray(new ProcStat[]{});
    wlist.setAdapter(new ArrayAdapter<ProcStat>(this, R.layout.ruleitem, wstats){
      @Override
      public View getView(int position, View convertView, ViewGroup parent)
      {
        View v = super.getView(position, convertView, parent);
        v.setTag(wlist);
        return v;
      }
    });

    bset = MonitorService.load(this, MonitorService.BLACK);
    bstats = bset.toArray(new ProcStat[]{});
    blist.setAdapter(new ArrayAdapter<ProcStat>(this, R.layout.ruleitem, bstats){
      @Override
      public View getView(int position, View convertView, ViewGroup parent)
      {
        View v = super.getView(position, convertView, parent);
        v.setTag(blist);
        return v;
      }
    });
  }

  @Override
  public boolean onContextItemSelected(MenuItem item)
  {
//    return super.onContextItemSelected(item);
    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
    Log.d("cpu", "removing item pos=" + info.position);
    ListView view = (ListView)info.targetView.getTag();
    ProcStat ps = (ProcStat)view.getAdapter().getItem(info.position);
    if (wset.contains(ps))
    {
      wset.remove(ps);
      MonitorService.save(this, MonitorService.WHITE, wset);
    }
    if (bset.contains(ps))
    {
      bset.remove(ps);
      MonitorService.save(this, MonitorService.BLACK, bset);
    }
    updateLists();
    return true;
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
  {
    super.onCreateContextMenu(menu, v, menuInfo);
//    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
//    String title = ((TextView) v.getItem(info.position)).getTitle();
//    menu.setHeaderTitle(title);

    menu.add(0, v.getId(), 0, "Detele");
  }
}


