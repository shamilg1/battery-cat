package im.gtm.batterycat;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.Observable;
import java.util.Observer;

public class BatteryCatActivity extends Activity implements Observer
{
  private TextView userCpuLoad;
  private Spinner cpuGuardModeSpinner;
  private SharedPreferences settings;
  private ToggleButton indicator;

  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    Button start, stop, showHist;
    start = (Button) findViewById(R.id.start);
    stop = (Button) findViewById(R.id.stop);
    showHist = (Button) findViewById(R.id.hist);
    userCpuLoad = (TextView) findViewById(R.id.cpuload);
    cpuGuardModeSpinner = (Spinner) findViewById(R.id.mode);
    indicator = (ToggleButton) findViewById(R.id.indicator);

    start.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View view)
      {
        startService(new Intent(MonitorService.ACTION));
      }
    });

    stop.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View view)
      {
        stopService(new Intent(MonitorService.ACTION));
      }
    });

    showHist.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View view)
      {
        startActivity(new Intent(BatteryCatActivity.this, RulesActivity.class));
      }
    });

    settings = getSharedPreferences(MonitorService.SETTINGS, MODE_PRIVATE);

    int mode = settings.getInt(MonitorService.MODE_KEY, MonitorService.VIGILANTE);
    if (mode < 0 || mode > MonitorService.MODES.length-1) mode = MonitorService.VIGILANTE;
    cpuGuardModeSpinner.setSelection(mode);

    cpuGuardModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
    {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int idx, long id)
      {
        if (idx == -1) idx = MonitorService.VIGILANTE;
        SharedPreferences.Editor ed = settings.edit();
        ed.putInt(MonitorService.MODE_KEY, idx);
        ed.apply();
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView)
      {
      }
    });
  }

  @Override
  protected void onResume()
  {
    super.onResume();
    IntentFilter f = new IntentFilter();
    f.addAction(MonitorService.CPU_LOAD_ACTION);
    registerReceiver(cpuloadReceiver, f, null, new Handler(Looper.getMainLooper()));
    //following code is thread-unsafe! todo god task of how to make it thread-safe?
    ((BatteryCatApplication)getApplicationContext()).serviceObservable.addObserver(this);
    showServiceEnabled(((BatteryCatApplication)getApplicationContext()).getService() != null);
  }

  @Override
  protected void onPause()
  {
    super.onPause();
    unregisterReceiver(cpuloadReceiver);
    ((BatteryCatApplication)getApplicationContext()).serviceObservable.deleteObserver(this);
  }

  private final BroadcastReceiver cpuloadReceiver = new BroadcastReceiver()
  {
    @Override
    public void onReceive(Context context, Intent intent)
    {
      int load = intent.getIntExtra("cpuload", -1);
      userCpuLoad.setText(""+load);
    }
  };

  @Override
  public void update(Observable observable, Object o)
  {
    if (o == null)
      showServiceEnabled(false);
    else
      showServiceEnabled(true);
  }

  private void showServiceEnabled(final boolean b)
  {
    runOnUiThread(new Runnable()
    {
      @Override
      public void run()
      {
        indicator.setChecked(b);
      }
    });
  }
}
